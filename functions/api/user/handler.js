const ClientError = require('../../exceptions/ClientError')
const UsersService = require('../../services/inPostgres/UsersService')
const { MapToModel } = require('../../utils/MapToModel')
const usersService = new UsersService()

exports.getAllUsersHandler = async function (req, res) {
    const users = await usersService.getAllUsers()

    res.json({
        results: users
    })
}

exports.postUserHandler = async function (req, res) {
    try {
        const payload = req.body

        const result = await usersService.addUser(MapToModel(payload))

        res.json({
            results: result
        })
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send(`Internal Server Error: ${error.message}`)
        }
    }
}

exports.putUserByIdHandler = async function (req, res) {
    try {
        const userId = req.params.id
        const payload = req.body

        const result = await usersService.editUserById(userId, MapToModel(payload))

        res.json({
            results: result
        })
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send('Internal Server Error')
        }
    }
}

exports.deleteUserByIdHandler = async function (req, res) {
    try {
        const userId = req.params.id

        await usersService.deleteUserById(userId)

        res.sendStatus(204)
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send('Internal Server Error')
        }
    }
}

exports.getUserByIdHandler = async function (req, res) {
    try {
        const userId = req.params.id

        const user = await usersService.getUserById(userId)

        res.json({
            results: user
        })
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send('Internal Server Error')
        }
    }
}
