const express = require('express')
const router = express.Router()
const UsersController = require('./handler')

router.get('/user', UsersController.getAllUsersHandler)

router.post('/user', UsersController.postUserHandler)
router.put('/user/:id', UsersController.putUserByIdHandler)
router.delete('/user/:id', UsersController.deleteUserByIdHandler)
router.get('/user/:id', UsersController.getUserByIdHandler)

module.exports = router
