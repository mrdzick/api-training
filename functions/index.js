/* eslint-disable no-undef */
require('dotenv').config()
const functions = require('firebase-functions')
const UsersService = require('./services/inPostgres/UsersService')
const usersService = new UsersService()
const ClientError = require('./exceptions/ClientError')
const { MapToModel } = require('./utils/MapToModel')
const Knex = require('knex')

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
let pool
const createTcpPool = async config => {
    // Extract host and port from socket address
    const dbSocketAddr = process.env.DB_HOST.split(':') // e.g. '127.0.0.1:5432'

    // Establish a connection to the database
    return Knex({
        client: 'pg',
        connection: {
            user: process.env.DB_USER, // e.g. 'my-user'
            password: process.env.DB_PASS, // e.g. 'my-user-password'
            database: process.env.DB_NAME, // e.g. 'my-database'
            host: dbSocketAddr[0], // e.g. '127.0.0.1'
            port: dbSocketAddr[1] // e.g. '5432'
        },
        // ... Specify additional properties here.
        ...config
    })
}

const createPool = async () => {
    // Configure which instance and what database user to connect with.
    // Remember - storing secrets in plaintext is potentially unsafe. Consider using
    // something like https://cloud.google.com/kms/ to help keep secrets secret.
    const config = { pool: {} }

    // [START cloud_sql_postgres_knex_limit]
    // 'max' limits the total number of concurrent connections this pool will keep. Ideal
    // values for this setting are highly variable on app design, infrastructure, and database.
    config.pool.max = 5
    // 'min' is the minimum number of idle connections Knex maintains in the pool.
    // Additional connections will be established to meet this value unless the pool is full.
    config.pool.min = 5
    // [END cloud_sql_postgres_knex_limit]

    // [START cloud_sql_postgres_knex_timeout]
    // 'acquireTimeoutMillis' is the number of milliseconds before a timeout occurs when acquiring a
    // connection from the pool. This is slightly different from connectionTimeout, because acquiring
    // a pool connection does not always involve making a new connection, and may include multiple retries.
    // when making a connection
    config.pool.acquireTimeoutMillis = 60000 // 60 seconds
    // 'createTimeoutMillis` is the maximum number of milliseconds to wait trying to establish an
    // initial connection before retrying.
    // After acquireTimeoutMillis has passed, a timeout exception will be thrown.
    config.pool.createTimeoutMillis = 30000 // 30 seconds
    // 'idleTimeoutMillis' is the number of milliseconds a connection must sit idle in the pool
    // and not be checked out before it is automatically closed.
    config.pool.idleTimeoutMillis = 600000 // 10 minutes
    // [END cloud_sql_postgres_knex_timeout]

    // [START cloud_sql_postgres_knex_backoff]
    // 'knex' uses a built-in retry strategy which does not implement backoff.
    // 'createRetryIntervalMillis' is how long to idle after failed connection creation before trying again
    config.pool.createRetryIntervalMillis = 200 // 0.2 seconds
    // [END cloud_sql_postgres_knex_backoff]

    return createTcpPool(config)
}

const insertVote = async (pool, vote) => {
    try {
        return await pool('votes').insert(vote)
    } catch (err) {
        throw Error(err)
    }
}

// const insertUser = async (pool, user) => {
//     try {
//         return await pool('users').insert(user)
//     } catch (err) {
//         throw Error(err)
//     }
// }

const getUsers = async pool => {
    return await pool
        .select('id', 'username', 'first_name', 'last_name', 'phone')
        .from('users')
        // .orderBy('time_cast', 'desc')
        // .limit(5)
}

const ensureSchema = async pool => {
    const hasTable = await pool.schema.hasTable('votes')
    const hasTableUser = await pool.schema.hasTable('users')
    if (!hasTable) {
        return pool.schema.createTable('votes', table => {
            table.increments('vote_id').primary()
            table.timestamp('time_cast', 30).notNullable()
            table.specificType('candidate', 'CHAR(6)').notNullable()
        })
    }
    if (!hasTableUser) {
        return pool.schema.createTable('users', table => {
            table.specificType('id', 'VARCHAR(50)').primary()
            table.specificType('username', 'VARCHAR(50)').notNullable()
            table.specificType('password', 'TEXT').notNullable()
            table.specificType('first_name', 'TEXT').notNullable()
            table.specificType('last_name', 'TEXT').notNullable()
            table.specificType('phone', 'TEXT').notNullable()
        })
    }
    functions.logger.info("Ensured that table 'votes' exists")
}

const createPoolAndEnsureSchema = async () =>
    await createPool()
        .then(async pool => {
            await ensureSchema(pool)
            return pool
        })
        .catch(err => {
            functions.logger.error(err)
            throw err
        })

exports.helloWorld = functions.https.onRequest(async (request, response) => {
    functions.logger.info('Hello logs!', { structuredData: true })
    pool = pool || (await createPoolAndEnsureSchema())
    // Get the team from the request and record the time of the vote.
    const { team } = request.body
    const timestamp = new Date()

    if (!team || (team !== 'TABS' && team !== 'SPACES')) {
        response.status(400).send('Invalid team specified.').end()
        return
    }

    // Create a vote record to be stored in the database.
    const vote = {
        candidate: team,
        time_cast: timestamp
    }

    // Save the data to the database.
    try {
        await insertVote(pool, vote)
    } catch (err) {
        functions.logger.error(`Error while attempting to submit vote:${err}`)
        response
            .status(500)
            .send('Unable to cast vote; see logs for more details.')
            .end()
        return
    }
    response.status(200).send(`Successfully voted for ${team} at ${timestamp}`).end()
    // response.send('Hello from Firebase!')
})

exports.getAllUsers = functions.https.onRequest(async (req, res) => {
    // const users = usersService.getAllUsers()
    pool = pool || (await createPoolAndEnsureSchema())
    const users = await getUsers(pool)
    res.json({
        results: users
    })
})

exports.postUser = functions.https.onRequest((req, res) => {
    try {
        const payload = req.body

        const result = usersService.addUser(MapToModel(payload))

        res.json({
            results: result
        })
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send(`Internal Server Error: ${error.message}`)
        }
    }
})

exports.putUserById = functions.https.onRequest((req, res) => {
    try {
        const userId = req.params.id
        const payload = req.body

        const result = usersService.editUserById(userId, MapToModel(payload))

        res.json({
            results: result
        })
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send('Internal Server Error')
        }
    }
})

exports.deleteUserById = functions.https.onRequest((req, res) => {
    try {
        const userId = req.params.id

        usersService.deleteUserById(userId)

        res.sendStatus(204)
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send('Internal Server Error')
        }
    }
})

exports.getUserById = functions.https.onRequest((req, res) => {
    try {
        const userId = req.params.id

        const user = usersService.getUserById(userId)

        res.json({
            results: user
        })
    } catch (error) {
        if (error instanceof ClientError) {
            res.status(error.statusCode).send(error.message)
        } else {
            res.status(500).send('Internal Server Error')
        }
    }
})
