require('dotenv').config()

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const userAPI = require('./api/user/routes')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/test', userAPI)

app.listen(process.env.PORT, () => {
    console.log(`Server berjalan pada port ${process.env.PORT}`)
})
