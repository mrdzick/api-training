const { Pool } = require('pg')
const { nanoid } = require('nanoid')
const bcrypt = require('bcrypt')
const InvarianError = require('../../exceptions/InvariantError')
const NotFoundError = require('../../exceptions/NotFoundError')

class UsersService {
    constructor () {
        this._pool = new Pool()
    }

    async addUser ({ username, password, firstName, lastName, phone }) {
        await this.verifyNewUsername(username)
        const id = `user-${nanoid(16)}`
        const hashedPassword = await bcrypt.hash(password, 10)

        const query = {
            text: 'INSERT INTO users VALUES($1, $2, $3, $4, $5, $6) RETURNING id',
            values: [id, username, hashedPassword, firstName, lastName, phone]
        }

        const result = await this._pool.query(query)

        if (!result.rowCount) {
            throw new InvarianError('User gagal ditambahkan')
        }

        return result.rows
    }

    async verifyNewUsername (username) {
        const query = {
            text: 'SELECT username FROM users WHERE username = $1',
            values: [username]
        }

        const result = await this._pool.query(query)

        if (result.rowCount > 0) {
            throw new InvarianError('Gagal menambahkan user. Username sudah digunakan!')
        }
    }

    async getAllUsers () {
        const result = await this._pool.query('SELECT id, username, first_name, last_name, phone FROM users')
        return result.rows
    }

    async getUserById (userId) {
        const query = {
            text: 'SELECT username, first_name, last_name, phone FROM users WHERE id=$1',
            values: [userId]
        }

        const result = await this._pool.query(query)

        if (!result.rowCount) {
            throw new NotFoundError('User tidak ditemukan')
        }

        return result.rows[0]
    }

    async editUserById (userId, payload) {
        const query = {
            text: 'UPDATE users SET first_name=$1, last_name=$2, phone=$3 WHERE id=$4 RETURNING first_name, last_name, phone',
            values: [payload.firstName, payload.lastName, payload.phone, userId]
        }

        const result = await this._pool.query(query)

        if (!result.rowCount) {
            throw new InvarianError('Gagal mengubah data')
        }

        return result.rows[0]
    }

    async deleteUserById (userId) {
        const query = {
            text: 'DELETE FROM users WHERE id=$1',
            values: [userId]
        }

        await this._pool.query(query)
    }
}

module.exports = UsersService
