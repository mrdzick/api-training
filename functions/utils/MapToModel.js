/* eslint-disable camelcase */
const MapToModel = ({
    username,
    password,
    first_name,
    last_name,
    phone
}) => ({
    username,
    password,
    firstName: first_name,
    lastName: last_name,
    phone
})

module.exports = { MapToModel }
